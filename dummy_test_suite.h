#ifndef DUMMY_TEST_SUITE
#define DUMMY_TEST_SUITE

#include <string.h>

#if 1
	#define DTS_FUNC_NAME __FUNCTION__
#else
	#define DTS_FUNC_NAME __PRETTY_FUNCTION__
#endif

#define TEST_FAIL_FLAG_CREATE() int __test_failed = 0;

#define TEST_FAIL( msg ) do { \
		++__test_failed; \
		if( strlen(msg) != 0 ) printf("%s\n", msg); \
		printf("[!] FAIL %s!\n", DTS_FUNC_NAME); \
	} while(0);

#define TEST_SUMMARY() do { \
		if( __test_failed == 0 ) \
		printf("[+++] Test %s passed.\n", DTS_FUNC_NAME); \
		else { \
		printf("[---] Test %s failed with %d errors.\n", DTS_FUNC_NAME, __test_failed); \
		} \
	} while(0);

#endif
