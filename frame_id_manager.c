#include "frame_id_manager.h"
#include "mesh_frame.h"
#include <assert.h>

struct addr_frame_id { 
	uint8_t addr; 
	uint8_t next_frame_id; 
};

static struct addr_frame_id addr_frame_id_map_addr[ MAX_DEVICES ];
static uint8_t addr_frame_id_map_addr_size = 0;

static int8_t priv_frame_id_mgr_locate_in_arr( uint8_t addr )
{
	int i = 0;
	for( i = 0; i < addr_frame_id_map_addr_size; ++i )
	{
		if( addr_frame_id_map_addr[ i ].addr == addr )
		{
			return i;
		}
	}

	return -1; // Not found.
}

// Check if received frame's frame ID is correct.
uint8_t frame_id_mgr_get_expected_id( uint8_t addr )
{
	int8_t id = priv_frame_id_mgr_locate_in_arr( addr );
	if( id == -1 )
	{
		// Dev addr not yet in local table, frame id should be 0
		// This is the first received frame.
		return 0;
	}

	return addr_frame_id_map_addr[ id ].next_frame_id;
}

// Get next ID for address.
uint8_t frame_id_mgr_get_next_id_for_addr( uint8_t addr )
{
	assert( addr_frame_id_map_addr_size < MAX_DEVICES );

	uint8_t id = priv_frame_id_mgr_locate_in_arr( addr );
	if( id != -1 )
	{
			uint8_t ret = addr_frame_id_map_addr[ id ].next_frame_id;
			++( addr_frame_id_map_addr[ id ].next_frame_id );
			return ret;
	}

	addr_frame_id_map_addr[ addr_frame_id_map_addr_size ].addr = addr;
	addr_frame_id_map_addr[ addr_frame_id_map_addr_size ].next_frame_id = 1;
	++addr_frame_id_map_addr_size;

	// Set current frame id to 0
	return 0;
}
