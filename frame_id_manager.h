#ifndef FRAME_ID_MANAGER_H
#define FRAME_ID_MANAGER_H

#include "types.h"

uint8_t frame_id_mgr_get_expected_id( uint8_t addr );
uint8_t frame_id_mgr_get_next_id_for_addr( uint8_t addr );

#endif
