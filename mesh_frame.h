#ifndef MESH_FRAME_H
#define MESH_FRAME_H

#include "types.h"

#define MAX_DEVICES 16

#define FRAME_CTRL_ACK_REQ_POS 			0
#define FRAME_CTRL_ACK_REPL_POS			1
#define FRAME_CTRL_BROADCAST_POS 		2
#define FRAME_CTRL_DISCOVERY_REQ_POS 	3
#define FRAME_CTRL_DISCOVERY_REPL_POS 	4

#define FRAME_BROADCAST_ADDR 0x00

struct mesh_frame
{
	uint8_t source_addr;
	uint8_t dest_addr;
	uint8_t frame_ctrl;
	uint8_t frame_id;
	uint8_t data_len;
	uint8_t data[16];
	uint8_t checksum;
};

#endif
