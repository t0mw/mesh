#include "mesh_network_discovery.h"
#include "mesh_frame.h"
#include "mesh_parser.h"
#include "types.h"
#include <stdio.h>
#include <stdlib.h>

#define TEST_MODE 1

#if TEST_MODE
	#include "windows_transport_network.h"
#endif

#define MESH_ND_READY 0x01 // TODO

void mesh_nd_send_req(uint8_t source_addr)
{
#if TEST_MODE
	// Should now remember, that we're in the state
	// of requesting a network discovery.
	// Additional variable for protocol state machine and a logic for
	// state switches needed here.

	struct mesh_frame fr;
	fr.source_addr = source_addr;
	fr.dest_addr = FRAME_BROADCAST_ADDR;
	fr.frame_ctrl = 1 << FRAME_CTRL_BROADCAST_POS |
					1 << FRAME_CTRL_DISCOVERY_REQ_POS;
	fr.frame_id = 0;
	fr.data_len = 0;

	uint8_t buf[32];
	uint8_t buf_size = 0;
	uint8_t ret = mesh_send_parse( &fr, buf, &buf_size );
	if( MESH_PARSE_OK != ret )
	{
		printf("Mesh send parse error: %d\n", ret );
		exit(-1);
	}

	int i = 0;
	for( i = 0; i < buf_size; ++i )
	{
		wtn_write( buf[i] );
	}
#endif
}

void mesh_nd_repl_req(uint8_t source_addr, uint8_t dest_addr)
{
#if TEST_MODE
	struct mesh_frame fr;
	fr.source_addr = source_addr;
	fr.dest_addr = dest_addr;
	fr.frame_ctrl = 1 << FRAME_CTRL_BROADCAST_POS |
					1 << FRAME_CTRL_DISCOVERY_REPL_POS;
	fr.frame_id = 0;
	fr.data_len = 0;

	uint8_t buf[32];
	uint8_t buf_size = 0;
	uint8_t ret = mesh_send_parse( &fr, buf, &buf_size );
	if( MESH_PARSE_OK != ret )
	{
		printf("Mesh send parse error: %d\n", ret );
		exit(-1);
	}

	int i = 0;
	for( i = 0; i < buf_size; ++i )
	{
		wtn_write( buf[i] );
	}
#endif
}

uint8_t mesh_nd_req_ready(void)
{
#if TEST_MODE
	return 0;
#endif
}

void mesh_nd_get_discovered(struct net_node *discovered, uint8_t *size)
{
#if TEST_MODE
#endif
}
