#ifndef MESH_NETWORK_DISCOVERY
#define MESH_NETWORK_DISCOVERY

#include "types.h"

#define MESH_ND_READY 0x01 // TODO

struct net_node
{
	uint8_t node_addr;
	uint8_t node_errors;
};

/*
 * Send "network discovery" request (broadcast).
*/
void mesh_nd_send_req( uint8_t source_addr );

/*
 * Reply to "network discovery" request, and
 * wait for ack.
*/
void mesh_nd_repl_req( uint8_t source_addr, uint8_t dest_addr );

/*
 * Check if network discovery request timer
 * expired and any device responded.
 * Returns:
 *	MESH_ND_READY
 *  MESH_ND_ERR_NOTREADY - timer not expired
 *	MESH_ND_ERR_NODEV - no devices responded
*/
uint8_t mesh_nd_req_ready( void );

/*
 * After sending network discovery request, call this function
 * to see what devices are visible by current node.
*/
void mesh_nd_get_discovered( struct net_node *discovered, uint8_t *size );

#endif
