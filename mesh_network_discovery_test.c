#include "mesh_network_discovery.h"
#include "dummy_test_suite.h"
#include "windows_transport_network.h"
#include "types.h"
#include "mesh_frame.h"
#include "mesh_parser.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

void TEST_simple_network_discovery( void )
{
	TEST_FAIL_FLAG_CREATE();
	
	// a0 <---> a1
	// a0 <---> a2
	// a1 <-/-> a2
	uint8_t a0_net_buf[32] = { 0 };
	uint8_t a0_net_buf_last = 0;

	// send broadcast net disc from a0
	// send response to a0 from a1 and a2
	mesh_nd_send_req( MESH_PARSER_CUR_DEV_ADDR );
	printf("Waiting for response...\n");
	//Sleep(10000);
	printf("Wait ended\n");

	// There is a problem with frame ID for the second
	// response - when the first response is send to CUR_DEV_ADDR,
	// the internal frame id manager will increment the
	// counter thats responsible for controlling the value of
	// frame id. After second reply to the same CUR_DEV_ADDR from
	// supposedly 0xa2 the expected frame id will be set to 2,
	// which is incorrect - it should be 1.
	//
	// Leave it as it is for now.
	mesh_nd_repl_req( 0xa1, MESH_PARSER_CUR_DEV_ADDR );
	mesh_nd_repl_req( 0xa2, MESH_PARSER_CUR_DEV_ADDR );

	// As a0, read the recv buffer and parse it.
	uint8_t ret = 1;
	struct mesh_frame recvd_frames[2];
	int recvd_frames_last = 0;
	do {
		uint8_t byte = 0x00;
		ret = wtn_read( &byte );
		if( ret != 0 )
		{
			printf("Read byte: %2x\n", byte);
			a0_net_buf[ a0_net_buf_last ] = byte;
			++a0_net_buf_last;

			uint8_t recvd_frames_count = 2 - recvd_frames_last;
			uint8_t ret_parse = mesh_recv_parse(recvd_frames + recvd_frames_last,
							&recvd_frames_count,
							a0_net_buf,
							a0_net_buf_last );
			if( ret_parse == 0 || recvd_frames_count > 0 )
			{
				printf("GOT VALID FRAME\n");
				++recvd_frames_last;	
				a0_net_buf_last = 0;
			}
		}
	} while( ret );

	exit(0);

	uint8_t nodes_count = 0;
	struct net_node nodes[2];
	uint8_t ret_code = mesh_nd_req_ready();
	if( ret_code == MESH_ND_READY )
	{
		mesh_nd_get_discovered( nodes, &nodes_count );
		uint8_t i = 0;
		for( i = 0; i < nodes_count; ++i )
			printf( "Discovered node: %d %02x\n", i, nodes[i].node_addr );
	}
	else
	{
		printf( "Error code: %d\n", ret_code );
		TEST_FAIL("");
	}
	
	TEST_SUMMARY();
}

int main( void )
{
	TEST_simple_network_discovery();

	return 0;
}
