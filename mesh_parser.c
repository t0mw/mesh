#include "mesh_parser.h"
#include "types.h"
#include "frame_id_manager.h"
#include <assert.h>

#define MESH_PROTOCOL_PREAMBLE_SIZE		2
#define MESH_PROTOCOL_PREAMBLE_BYTE		0x51
// 1 + 1 + 1 + 1 + 1 (checksum, source addr, dest addr, ...)
#define MESH_PROTOCOL_OVERLOAD			(5 + MESH_PROTOCOL_PREAMBLE_SIZE)

uint8_t mesh_recv_parse( struct mesh_frame *frame,
						 uint8_t *frame_count,
						 const uint8_t *data, 
						 const uint8_t data_len )
{
	assert( frame != NULL && data != NULL );

	uint8_t i = 0; // Frame data field iterator.
	uint8_t j = 0; // Number of frames iterator.
	// Find preamble in the "data" buffer, then
	// address all the relevant bytes by the offset.
	uint8_t offset = 0;
	uint8_t preamble_to_read_left = MESH_PROTOCOL_PREAMBLE_SIZE;
	uint8_t last_error = MESH_PARSE_OK;
	uint8_t actual_frames_parsed = 0;
	// The max frame count is saved in in/out parameter
	// frame_count, but it will be destroyed, save it.
	uint8_t max_frame_count = *frame_count;
	*frame_count = 0;

	if( data_len > ( MESH_RECV_BUF_SIZE + MESH_PROTOCOL_OVERLOAD ) )
		return MESH_PARSE_BUF_TOO_SMALL;

	for( ; actual_frames_parsed < max_frame_count; )
	{
		j = actual_frames_parsed;
		for( ; offset < data_len; ++offset )
		{
			if(data[offset] == MESH_PROTOCOL_PREAMBLE_BYTE)
				--preamble_to_read_left;

			if( preamble_to_read_left == 0 )
			{
				preamble_to_read_left = MESH_PROTOCOL_PREAMBLE_SIZE;
				++offset;
				break;
			}
		}

		if( offset >= data_len )
		{
			last_error = MESH_PARSE_ERR_NO_PREAMBLE;
			break;
		}

		frame[j].source_addr = data[ offset + 0 ];
		uint8_t checksum = frame[j].source_addr;	// Checskum init
		frame[j].dest_addr = data[ offset + 1 ];
		checksum ^= frame[j].dest_addr;
		frame[j].frame_ctrl = data[ offset + 2 ];
		checksum ^= frame[j].frame_ctrl;
		frame[j].frame_id = data[ offset + 3 ];
		checksum ^= frame[j].frame_id;
		frame[j].data_len = data[ offset + 4 ];
		checksum ^= frame[j].data_len;

		if( frame[j].data_len >= MESH_RECV_BUF_SIZE )
		{
			last_error = MESH_PARSE_ERR_LEN; // error invalid data len
			continue;
		}

		for( i = 0; i < frame[j].data_len; ++i )
		{
			frame[j].data[ i ] = data[ offset + 5 + i ];
			checksum ^= frame[j].data[ i ];
		}

		// Print correct chcecksum, for debugging.
		// printf( "Valid checksum: %02x", checksum );

		if( ( offset + 5 + frame[j].data_len ) > data_len )
		{
			last_error = MESH_PARSE_BUF_TOO_SMALL;
			continue;
		}

		if( data[ offset + 5 + frame[j].data_len ] != checksum )
		{
			last_error = MESH_PARSE_ERR_CHECKSUM; // error invalid checksum
			continue;
		}

		if( frame[j].frame_id != frame_id_mgr_get_expected_id( frame[j].source_addr ) )
		{
			last_error = MESH_PARSE_ERR_FRAME_ID; // error invalid checksum
			continue;
		}

		if( frame[j].source_addr == MESH_PARSER_CUR_DEV_ADDR )
		{
			last_error = MESH_PARSE_ERR_RECEIVED_SELF_MSG;
			continue;
		}

		frame[j].checksum = data[ offset + 5 + frame[j].data_len ];

		// All error checks passed, this is a valid frame.
		// Start looking for a next frame in this buffer from the
		// end of current buffer position.
		offset = offset + 5 + frame[j].data_len;
		last_error = MESH_PARSE_OK;
		++actual_frames_parsed;
	}

	*frame_count = actual_frames_parsed;
	
	return last_error;
}

/*
	Assign frame ID for the specified address in the mesh_frame
	structure. Every address has a range of 0-255 frame id's.
*/
static void mesh_assign_frame_id( struct mesh_frame *frame )
{
	if( frame->dest_addr == FRAME_BROADCAST_ADDR )
		frame->frame_id = 0;
	else
		frame->frame_id = frame_id_mgr_get_next_id_for_addr( frame->dest_addr );
}

uint8_t mesh_send_parse( struct mesh_frame *frame,
						 uint8_t *data, 
						 uint8_t *data_size )
{
	assert( data != NULL && data_size != NULL );

	if( frame->data_len >= MESH_RECV_BUF_SIZE )
		return MESH_PARSE_ERR_LEN; // error invalid data len

	mesh_assign_frame_id( frame );

	uint8_t i = 0;
	for( i = 0; i < MESH_PROTOCOL_PREAMBLE_SIZE; ++i )
		data[ i ] = MESH_PROTOCOL_PREAMBLE_BYTE;

	// Address data with preamble size offset.
	uint8_t offset = MESH_PROTOCOL_PREAMBLE_SIZE;

	data[ offset + 0 ] = frame->source_addr;
	uint8_t checksum = frame->source_addr;	// Checskum init
	data[ offset + 1 ] = frame->dest_addr;
	checksum ^= frame->dest_addr;
	data[ offset + 2 ] = frame->frame_ctrl;
	checksum ^= frame->frame_ctrl;
	data[ offset + 3 ] = frame->frame_id;
	checksum ^= frame->frame_id;
	data[ offset + 4 ] = frame->data_len;
	checksum ^= frame->data_len;

	for( i = 0; i < frame->data_len; ++i )
	{
		data[ offset + 5 + i ] = frame->data[ i ];
		checksum ^= frame->data[ i ];
	}

	data[ offset + 5 + frame->data_len ] = checksum;

	*data_size = offset + 5 + frame->data_len + 1;

	return MESH_PARSE_OK;
}
