#ifndef MESH_RECEIVER_H
#define MESH_RECEIVER_H

#include "mesh_frame.h"
#include "types.h"

#define MESH_PARSER_CUR_DEV_ADDR 0xa0

#define MESH_RECV_BUF_SIZE 16
#define MESH_SEND_BUF_SIZE 16

#define MESH_PARSE_OK						0x00
#define MESH_PARSE_ERR_LEN 					0x01
#define MESH_PARSE_ERR_CHECKSUM 			0x02
// Expected different frame ID from source address of this frame.
#define MESH_PARSE_ERR_FRAME_ID				0x03
// Received message from address of the current dev, something's not right.
#define MESH_PARSE_ERR_RECEIVED_SELF_MSG	0x04
#define MESH_PARSE_ERR_NO_PREAMBLE			0x05
// The buffer is too small for a serialized valid frame.
#define MESH_PARSE_BUF_TOO_SMALL			0x06

/*
 * Returns: 
 * 	MESH_PARSE_OK
 * 	MESH_PARSE_ERR_LEN
 * 	MESH_PARSE_ERR_CHECKSUM
 *
 * Can handle several frames inside *data. Will return
 *	number of detected frames in frame_count, "frame"
 *	should be an array that can handle at least *frame_count
 *	frames. If there will be any error the parsing will continue.
 *	If there was a frame in buffer but the frame was invalid, the
 *	returned code will be the last error that was detected.
 * Pass already allocated "frame" of size "*frame_count", this
 *	function won't allocate more frames inside "frame" that "*frame_count"
 *	and will modify this parameter to the actual number of parsed frames.
 */
uint8_t mesh_recv_parse( struct mesh_frame *frame,
						 uint8_t *frame_count,
						 const uint8_t *data, 
						 const uint8_t data_len );

/*
 * Returns:
 *  MESH_PARSE_OK
 *  MESH_PARSE_ERR_LEN
 */
uint8_t mesh_send_parse( struct mesh_frame *frame,
						 uint8_t *data, 
						 uint8_t *data_size );
 
#endif
