#include "mesh_parser.h"
#include "mesh_frame.h"
#include "types.h"
#include "dummy_test_suite.h"
#include <stdio.h>

void TEST_recv_parser_valid( void )
{
	TEST_FAIL_FLAG_CREATE();

	const uint8_t valid_data[] = { 0xa0, 				// src addr
								   0xb0, 				// dest addr
								   0x01,				// frame control
								   0xab,				// frame id
								   0x03,				// data len
								   0xa0, 0xa1, 0xa2,	// data
								   0x1a,				// checksum
								 };
	const uint8_t valid_data_size = sizeof(valid_data)/sizeof(*valid_data);

	struct mesh_frame frame = { 0 };
	uint8_t frame_count = 1;
	uint8_t err_code = mesh_recv_parse( &frame, &frame_count, valid_data, valid_data_size );
	if( err_code != MESH_PARSE_OK )
	{
		printf("Error code: %d\n", err_code);
		TEST_FAIL("");
	}

	if( frame.source_addr != valid_data[0] )
		TEST_FAIL( "Source address invalid!" );

	if( frame.dest_addr != valid_data[1] )
		TEST_FAIL( "Dest address invalid!" );

	if( frame.frame_ctrl != valid_data[2] )
		TEST_FAIL( "Frame control byte invalid!" );

	if( frame.frame_id != valid_data[3] )
		TEST_FAIL( "Frame control byte invalid!" );

	if( frame.data_len != valid_data[4] )
		TEST_FAIL( "Data len byte invalid!" );

	int i = 0;
	for( i = 0; i < frame.data_len; ++i )
	{
		if( frame.data[ i ] != valid_data[5 + i] )
		{
			printf( "(is) %02x != (should be) %02x\n", frame.data[ i ],valid_data[5 + i] );
			TEST_FAIL( "Data bytes invalid!" );
		}
	}

	if( frame.checksum != valid_data[ 5 + frame.data_len ] )
	{
		printf( "(is) %02x != (should be) %02x\n", frame.checksum, valid_data[ 5 + frame.data_len ] );
		TEST_FAIL( "Checksum invalid!" );
	}
	
	TEST_SUMMARY();
}

void TEST_recv_parser_size_invalid( void )
{
	TEST_FAIL_FLAG_CREATE();

	const uint8_t valid_data[] = { 0xa0, 				// src addr
								   0xb0, 				// dest addr
								   0x01,				// frame control
								   0xab,				// frame id
								   0x10,				// data len (invalid)
								   0xa0, 0xa1, 0xa2,	// data
								   0xb1,				// checksum (invalid)
								 };
	const uint8_t valid_data_size = sizeof(valid_data)/sizeof(*valid_data);

	struct mesh_frame frame = { 0 };
	uint8_t frame_count = 1;
	uint8_t err_code = mesh_recv_parse( &frame, &frame_count, valid_data, valid_data_size );
	if( err_code != MESH_PARSE_ERR_LEN )
	{
		printf("Error code: %d\n", err_code);
		TEST_FAIL( "Invalid len accepted as valid!" );
	}

	TEST_SUMMARY();
}

void TEST_recv_parser_checksum_invalid( void )
{
	TEST_FAIL_FLAG_CREATE();

	const uint8_t valid_data[] = { 0xa0, 				// src addr
								   0xb0, 				// dest addr
								   0x01,				// frame control
								   0xab,				// frame id
								   0x03,				// data len
								   0xa0, 0xa1, 0xa2,	// data
								   0xcc,				// checksum
								 };
	const uint8_t valid_data_size = sizeof(valid_data)/sizeof(*valid_data);

	struct mesh_frame frame = { 0 };
	uint8_t frame_count = 1;
	uint8_t err_code = mesh_recv_parse( &frame, &frame_count, valid_data, valid_data_size );
	if( err_code != MESH_PARSE_ERR_CHECKSUM )
	{
		printf("Error code: %d\n", err_code);
		TEST_FAIL( "Invalid checksum accepted as valid!" );
	}

	TEST_SUMMARY();
}

void TEST_recv_parser_no_data( void )
{
	TEST_FAIL_FLAG_CREATE();

	const uint8_t valid_data[] = { 0xa0, 				// src addr
								   0xb0, 				// dest addr
								   0x01,				// frame control
								   0xab,				// frame id
								   0x00,				// data len
								   0xba,				// checksum
								 };
	const uint8_t valid_data_size = sizeof(valid_data)/sizeof(*valid_data);

	struct mesh_frame frame = { 0 };
	uint8_t frame_count = 1;
	uint8_t err_code = mesh_recv_parse( &frame, &frame_count, valid_data, valid_data_size );
	if( err_code != MESH_PARSE_OK )
	{
		printf("Error code: %d\n", err_code);
		TEST_FAIL( "" );
	}

	TEST_SUMMARY();
}

void TEST_send_parser_valid( void )
{
}

//int main( void )
//{
//	TEST_recv_parser_valid();
//	TEST_recv_parser_size_invalid();
//	TEST_recv_parser_checksum_invalid();
//	TEST_recv_parser_no_data();
//
//	return 0;
//}

