#include "mesh_state_machine.h"
#include "types.h"

// The next state depends on several conditions.
#define DEPENDS 0

static enum msm_state msm_current_state;

void msm_goto_next_state( enum msm_event e )
{
    switch( e )
    {
        case MSM_EVENT_SEND_NET_DISCO_TIME:
        {
            if(msm_current_state != MSM_STATE_IDLE)
                return; // Do it later. The event is cyclic, will remember about itself.

            // Setup event to send the net disco

            msm_current_state = MSM_STATE_WAIT_NET_DISCO_RESPONSE;
        }
        break;

        case MSM_EVENT_RECVD_NET_DISCO_REPLY:
        {
            if(msm_current_state != MSM_STATE_WAIT_NET_DISCO_RESPONSE)
                return; // Later.

            //  Where is the address of replying device?
            //  Where is the list of devices in neighborhood?
            //  Setup event to send the ack.

            msm_current_state = MSM_STATE_RECVD_NET_DISCO_RESPONSE_HANDLING;
        }
        break;

        case MSM_EVENT_RECV_NET_DISCO_RESPONSE_TIMEOUT:
        {
            if (msm_current_state != MSM_STATE_WAIT_NET_DISCO_RESPONSE)
                return;
            msm_current_state = MSM_STATE_IDLE;
        }
        break;

        case MSM_EVENT_PING_ALL_TIME:
        {
            if (msm_current_state != MSM_STATE_IDLE)
                return;

            // Setup event to ping all neighbors.

            msm_current_state = MSM_STATE_WAIT_PING_REPLY;
        }
        break;

        case MSM_EVENT_RECVD_PING_REPLY:
        {
            msm_current_state = MSM_STATE_RECVD_PING_HANDLING;
        }
        break;

        case MSM_EVENT_PING_REPLY_TIMEOUT: break;
        case MSM_EVENT_DATA_SENT: break;
        case MSM_EVENT_DATA_SENT_ACK: break;
        case MSM_EVENT_DATA_SENT_TIMEOUT: break;
        case MSM_EVENT_RECVD_NET_DISCO_REQ: break;
        case MSM_EVENT_NET_DISCO_REQ_ACK: break;
        case MSM_EVENT_NET_DISCO_REQ_TIMEOUT: break;
        case MSM_EVENT_RECVD_PING: break;
        case MSM_EVENT_RECV_DATA: break;
        default:
            assert(NULL == "invalid event code");
            break;
    }
}

enum msm_state msm_get_current_state( void )
{
    return msm_current_state;
}
