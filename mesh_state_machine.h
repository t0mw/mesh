#ifndef MESH_STATE_MACHINE_H
#define MESH_STATE_MACHINE_H

/*
 * This file defines API, states and actions for the
 * mesh state machine. On every packet recv/send there is
 * required some action - it's easy to manage those by proper state
 * transitions.
 */

// TODO:
// Retransmissions ?

enum msm_event
{
	// It's time to send the net discovery packet.
	MSM_EVENT_SEND_NET_DISCO_TIME,
	// Received network discovery response.
	MSM_EVENT_RECVD_NET_DISCO_REPLY,
	// Net disco time's up.
	MSM_EVENT_RECV_NET_DISCO_RESPONSE_TIMEOUT,

	// Send ping to all neighbor nodes.
	MSM_EVENT_PING_ALL_TIME,
	MSM_EVENT_RECVD_PING_REPLY,
	MSM_EVENT_PING_REPLY_TIMEOUT,

	// Just sent a data packet, waiting for ack.
	MSM_EVENT_DATA_SENT,
	MSM_EVENT_DATA_SENT_ACK,
	MSM_EVENT_DATA_SENT_TIMEOUT,

	// Just received a net disco packet.
	MSM_EVENT_RECVD_NET_DISCO_REQ,
	// Replied for a net disco request, got ack.
	MSM_EVENT_NET_DISCO_REQ_ACK,
	MSM_EVENT_NET_DISCO_REQ_TIMEOUT,

	// Received a ping.
	MSM_EVENT_RECVD_PING,

	// Received a data packet.
	MSM_EVENT_RECV_DATA
};

/* add events:
    - ping handling finished
*/

enum msm_state
{
	MSM_STATE_IDLE,
	// Waiting for the net disco response packet.
	MSM_STATE_WAIT_NET_DISCO_RESPONSE,
	// Got net disco response, handling it currently.
	MSM_STATE_RECVD_NET_DISCO_RESPONSE_HANDLING,

	// Just sent the ping to one of the neighbors, waiting for response.
	MSM_STATE_WAIT_PING_REPLY,
	MSM_STATE_RECVD_PING_HANDLING,
	MSM_STATE_PING_TIMEOUT_HANDLING,

	// Just sent a data packet, wait for ack/timeout.
	MSM_STATE_DATA_SENT_WAIT_ACK,
	MSM_STATE_DATA_SENT_ACK_HANDLING,
	MSM_STATE_DATA_SENT_TIMEOUT_HANDLING,

	// Just received a net disco packet, reply ASAP.
	MSM_STATE_NET_DISCO_REQ_HANDLING,
	// Replied to a net disco request, wait for ack.
	MSM_STATE_NET_DISCO_REPLY_WAIT_ACK,
	MSM_STATE_NET_DISCO_REPLY_ACK_RECV_HANDLING,
	MSM_STATE_NET_DISCO_REPLY_TIMEOUT_HANDLING,

	// Replying to a ping
	MSM_STATE_PING_REPLY_HANDLING,

	// Received data packet, send ACK.
	MSM_STATE_RECV_DATA_HANDLING
};

void msm_goto_next_state( enum msm_event e );
enum msm_state msm_get_current_state( void );

#endif
