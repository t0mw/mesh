#include "ring_buf.h"
#include <assert.h>

void ring_init( struct ring_buf *r )
{
	assert( r != NULL );

	r->start_idx = 0;
	r->end_idx = 0;
	r->len = 0;
}

void ring_add_byte( struct ring_buf *r, uint8_t byte )
{
	assert( r != NULL );

	if( r->len == RING_BUF_MAX )
	{
		// Buffer full, add anyway with shift of
		// the start/end indexes.
		r->start_idx = ( r->start_idx + 1 ) % RING_BUF_MAX;
	}
	else
		++( r->len );
	
	r->data[ r->end_idx ] = byte;
	r->end_idx = ( r->end_idx + 1 ) % RING_BUF_MAX;
}

uint8_t ring_get_byte( struct ring_buf *r )
{
	assert( r != NULL );
	
	uint8_t b = r->data[ r->start_idx ];
	r->start_idx = ( r->start_idx + 1 ) % RING_BUF_MAX;
	
	--( r->len );
	
	return b;
}

uint8_t ring_get_size( struct ring_buf *r )
{
	assert( r != NULL );

	return r->len;
}

void ring_read( struct ring_buf *r, uint8_t *dest, uint8_t size )
{
	assert( r != NULL );

	// Do a smart copy - memcpy if possible.
	int i = 0;
	uint8_t to_read = size < r->len ? size : r->len; // min(size, r->len)
	for( i = 0; i < to_read; ++i )
	{
		dest[i] = ring_get_byte(r);
	}
}
