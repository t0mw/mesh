#ifndef RING_BUF_H
#define RING_BUF_H

#include "types.h"

#define RING_BUF_MAX 16

struct ring_buf
{
	uint8_t data[ RING_BUF_MAX ];
	uint8_t start_idx;
	uint8_t end_idx;
	uint8_t len;
};

void ring_init( struct ring_buf *r );
void ring_add_byte( struct ring_buf *r, uint8_t byte );
uint8_t ring_get_byte( struct ring_buf *r );
uint8_t ring_get_size( struct ring_buf *r );
void ring_read( struct ring_buf *r, uint8_t *dest, uint8_t size );

#endif
