#include "ring_buf.h"
#include "types.h"
#include "dummy_test_suite.h"
#include <stdio.h>

void TEST_ring_simple_write_read( void )
{
	TEST_FAIL_FLAG_CREATE();

	struct ring_buf r;
	ring_init( &r );

	int i = 0;
	for( i = 0; i < RING_BUF_MAX; ++i )
		ring_add_byte( &r, ( uint8_t )( i % 10 ) );

	for( i = 0; i < RING_BUF_MAX; ++i )
	{
		uint8_t ring_b = ring_get_byte( &r );
		if( ring_b != ( uint8_t )( i % 10 ) )
		{
			printf( "i = %d, read = %02x, should_be = %02x\n", 
					i, 
					ring_b,
					( uint8_t )( i % 10 ) );
			TEST_FAIL( "Read byte is not equal to wrote byte." )
		}
	}

	TEST_SUMMARY();
}

void TEST_ring_size( void )
{
	TEST_FAIL_FLAG_CREATE();

	struct ring_buf r;
	ring_init( &r );
	
	int i = 0;
	for( i = 0; i < RING_BUF_MAX; ++i )
	{
		ring_add_byte( &r, 0xAB );
		uint8_t ring_size = ring_get_size( &r );
		if( ring_size != ( i + 1 ) )
		{
			printf( "Ring size invalid: is %d should be %d\n", ring_size, i + 1 );
			printf( "RING start: %d, end: %d\n", r.start_idx, r.end_idx );
			TEST_FAIL("add RING_BUF_MAX bytes");
		}
	}

	for( i = 0; i < RING_BUF_MAX - 3; ++i )
	{
		ring_add_byte( &r, ( uint8_t )( i % 10 ) );
		uint8_t ring_size = ring_get_size( &r );
		if( ring_size != 16 )
		{
			printf( "Ring size invalid: is %d should be %d\n", ring_size, 16 );
			printf( "RING start: %d, end: %d\n", r.start_idx, r.end_idx );
			TEST_FAIL("Add RING_BUF_MAX - 3 bytes to full ring");
		}
	}

	for( i = RING_BUF_MAX; i >= 0; --i )
	{
		uint8_t ring_size = ring_get_size( &r );
		if( ring_size != i )
		{
			printf( "Ring size invalid: is %d should be %d\n", ring_size, i );
			printf( "RING start: %d, end: %d\n", r.start_idx, r.end_idx );
			TEST_FAIL("Remove RING_BUF_MAX bytes");
		}

		( void )ring_get_byte( &r );
	}

	TEST_SUMMARY();
}

void TEST_ring_continuous_write_read( void )
{
	TEST_FAIL_FLAG_CREATE();

	struct ring_buf r;
	uint8_t ring_size = 0;
	ring_init( &r );

	// Write 5, read 3, Write 5, read 2, Write 5, read 1
	// Write values from 0 - 9
	int i = 0;
	for( i = 0; i < 5; ++i )
		ring_add_byte( &r, ( uint8_t )( i % 10 ) );
	for( i = 0; i < 3; ++i )
	{
		uint8_t b = ring_get_byte( &r );
		if( b != ( uint8_t )( i % 10 ) )
		{
			TEST_FAIL( "read check #1" )
		}
	}
	ring_size = ring_get_size( &r );
	if( ring_size != 2 )
		TEST_FAIL("size check #1");

	for( i = 0; i < 5; ++i )
		ring_add_byte( &r, ( uint8_t )( i % 10 ) );
	for( i = 0; i < 2; ++i )
	{
		uint8_t b = ring_get_byte( &r );
		if( b != ( uint8_t )( ( i + 3 ) % 10 ) )
			TEST_FAIL( "read check #2" )
	}
	ring_size = ring_get_size( &r );
	if( ring_size != 5 )
		TEST_FAIL("size check #2");

	for( i = 0; i < 5; ++i )
		ring_add_byte( &r, ( uint8_t )( i % 10 ) );
	for( i = 0; i < 1; ++i )
	{
		uint8_t b = ring_get_byte( &r );
		if( b != ( uint8_t )( i % 10 ) )
			TEST_FAIL( "read check #3" );
	}
	ring_size = ring_get_size( &r );
	if( ring_size != 9 )
		TEST_FAIL("size check #3");

	TEST_SUMMARY();
}

void TEST_ring_continuous_write_read_overflow( void )
{
	TEST_FAIL_FLAG_CREATE();

	struct ring_buf r;
	ring_init( &r );

	// for i times write&read j bytes from ring
	int i = 0, j = 0, k = 0;
	int read_size_max = RING_BUF_MAX;

	for( i = 0; i < RING_BUF_MAX + 3; ++i )
	{
		for( j = 1; j < read_size_max; ++j )
		{
			for( k = 0; k < j; ++k )
			{
				ring_add_byte( &r, k % 10 );
			}

			for( k = 0; k < j; ++k )
			{
				uint8_t b = ring_get_byte( &r );
				if( b != k % 10 )
				{
					TEST_FAIL("byte comparison");
				}
			}

			if( ring_get_size( &r ) != 0 )
			{
				TEST_FAIL("size comparison");
			}
		}
	}

	TEST_SUMMARY();
}

//int main( void )
//{
//	TEST_ring_simple_write_read();
//	TEST_ring_size();
//	TEST_ring_continuous_write_read();
//	TEST_ring_continuous_write_read_overflow();
//	
//	return 0;
//}
