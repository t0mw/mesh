#ifndef TIMER_H
#define TIMER_H

#include "types.h"

/*
 * Timestamp structure. If the "seconds" value is too big
 * for a uint32_t type, the "overflows" field should be incremented
 * and seconds should be zeroed.
 */
struct timer_stamp
{
	uint32_t overflows;
	uint32_t seconds;
};

struct timer
{
	struct timer_stamp start_timestamp;	// Timestamp set after timer_start call.
	uint32_t seconds;					// Seconds the timer should count.
};

/*
 * Get the time that passed from start of the system.
 */
struct timer_stamp *time_from_boot_get( void );

/*
 * Increment the time from boot. Should be called
 * about every second.
 *
 * CURRENT CALL FREQUENCY SHOULD BE: 1 second (1 Hz)
 */
void time_from_boot_increment( void );

/*
 * Starts the timer - saves the appropriate time stamp in the "t"
 * field, sets the "
 */
void timer_start( struct timer *t, uint32_t seconds );

void timer_terminate( struct timer *t );

/*
 * Returns "1" if timer expired, "0" otherwise.
 */
uint8_t timer_expired( struct timer *t );

#endif
