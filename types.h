#ifndef TYPES_H
#define TYPES_H

#ifndef NULL
	#define NULL ((void *)0)
#endif

typedef unsigned	char			uint8_t;
typedef signed		char			int8_t;
typedef unsigned	long long int	uint32_t;

#endif
