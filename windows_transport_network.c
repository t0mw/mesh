#include "windows_transport_network.h"

#include <stdio.h>
#include <Windows.h>
#include <assert.h>

static int size = 0;
HANDLE shared_file_write = NULL;
HANDLE shared_file_read = NULL;

void wtn_write(uint8_t byte)
{
	if (shared_file_write == NULL)
	{
		shared_file_write = CreateFile("_tmp_windows_transport_net",
			FILE_APPEND_DATA,
			FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		if (shared_file_write == NULL)
		{
			printf("Create shared file error\n");
			exit(-1);
		}
	}

	BOOL ret = WriteFile(shared_file_write,
						&byte,
						1,
						NULL,
						NULL);
	if (ret != TRUE)
	{
		printf("WriteFile error\n");
		exit(-2);
	}
}

uint8_t wtn_read(uint8_t *byte)
{
	if (shared_file_read == NULL)
	{
		shared_file_read = CreateFile("_tmp_windows_transport_net",
			GENERIC_READ,
			FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		if (shared_file_read == NULL)
		{
			printf("Create shared file error\n");
			exit(-1);
		}
	}

	if (shared_file_read == NULL || wtn_size() == 0)
	{
		return 0;
	}

	unsigned int read = 0;
	BOOL ret = ReadFile(shared_file_read,
		byte,
		1,
		(LPDWORD)&read,
		NULL);

	if (ret != TRUE || read == 0)
	{
		return 0;
	}

	return 1;
}

uint8_t wtn_size(void)
{
	if (shared_file_write == NULL)
	{
		printf("No size yet\n");
		return 0;
	}

	int file_size = GetFileSize(shared_file_write,
		NULL);

	// Should never be bigger than 0xff
	assert(file_size < 255);

	return (uint8_t)file_size;
}
