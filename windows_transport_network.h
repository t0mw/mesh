#ifndef WINDOWS_TRANSPORT_NETWORK_H
#define WINDOWS_TRANSPORT_NETWORK_H

#include "types.h"

void wtn_write(uint8_t byte);

/* Returns 0 if no data to read */
uint8_t wtn_read( uint8_t *byte );
uint8_t wtn_size(void);

#endif
